FROM golang:alpine as builder
ARG VERSION
WORKDIR /build
RUN apk add --no-cache curl tar gcc ca-certificates libc-dev
RUN curl -sSL https://github.com/librespeed/speedtest-go/archive/${VERSION}.tar.gz | tar xz --strip 1
RUN GOFLAGS="-buildvcs=false" CGO_ENABLED=0 GOOS=linux go build -a -ldflags "-w -s" -trimpath -o speedtest .
RUN sed -i 's/assets_path=""/assets_path="assets"/g' settings.toml && \
    sed -i 's@this._serverList = \[\]@this._serverList = \[{name: window.location.host, server:window.location.protocol+"//"+window.location.host+"/", dlURL:"garbage", ulURL:"empty", pingURL:"empty", getIpURL:"getIP"}\]@g' web/assets/speedtest.js && \
    sed -i 's/LibreSpeed Example/LibreSpeed SpeedTest Go/g' web/assets/example-singleServer-pretty.html && \
    mv web/assets/example-singleServer-pretty.html web/assets/index.html

FROM alpine
ENV TZ="UTC" \
    UID=1000 \
    GID=1000
RUN apk --no-cache add ca-certificates su-exec && \
    mkdir /config \
WORKDIR /app
COPY --from=builder /build/speedtest /build/settings.toml /app/
COPY --from=builder /build/web/assets/index.html /build/web/assets/*.js /app/assets-default/
COPY entrypoint.sh /app/
ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["/app/speedtest", "-c", "/config/settings.toml"]
